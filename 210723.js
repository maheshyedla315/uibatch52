//sum of factors of a number

function sumoffactors(num){
    let count =0;
    for(let i =1; i<=num/2; i++){
        if(num%i==0){
            count=count+i;
        }
    }
    return count+num;
}
console.log(sumoffactors(28))

//perfect number

function perfect(num){

    count=0
    for(i=1;i<=num/2;i++){
        if(num%i==0){
            count=count+i
            
        }
    
    }
    if(num==count){
        return "perfect number"
    }
    else{
        return "not a perfect number"
    }

}
console.log(perfect(27))

//array method
//.push()

let array = ['sai' , 'mahesh' , 'yedla']


array.push('last'); 

console.log(array) 

//.pop()

let charArray = ['sai' , 'mahesh' , 'yedla']


array.pop('last'); 

console.log(charArray) 

// take a sentence as input and write a function in which count the words in the given setence

function countWords(sentence) {
    sentence = sentence.trim();
    if (sentence === "") {
      return 0;
    }
    const words = sentence.split(" ");
    return words.length;
  }
  const sentence = "Hello i am learning javascript ";
console.log("the number of words are"+" "+countWords(sentence)); 
  
// count Sum of the negative values in an given array

function sumOfNegatives(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] < 0) {
        sum += arr[i];
      }
    }
    return sum;
  }
  const array1 = [-1, 2, 3,-2, -5, 10];
console.log(sumOfNegatives(array1)); 

//Problem1
//Write a program to find whether the given substring is present in the given sentence
//Input: "Hello world" ,world
//Output:true


//Problem2
//You have been given an array of strings and a specified length value.
//Write a function that filters the string from the array, Keeping those strings 
//whose length is greater than the  specified length.

//Input: ["java","Python","HTMl","CSS"] ,4
//Output should return the array elements  >4
//["Python"]

//prepare map,filter,reduce

// H.W

function isSubstringPresent(sentence, substring) {
  return sentence.includes(substring);
}

const inputSentence = "Hello world";
const inputSubstring = "world";
const output1 = isSubstringPresent(inputSentence, inputSubstring);
console.log(output1);

function filterStringsByLength(arr, length) {
  return arr.filter(str => str.length > length);
}
const inputArray = ["java", "Python", "HTMl", "CSS"];
const specifiedLength = 4;
const output = filterStringsByLength(inputArray, specifiedLength);
console.log(output);

let numbers = [11,22,33,44,55]
let words = ["hello","hii","bye","yeah"]
let cubes = numbers.map((num)=>num*num*num)
let oddnumbers = numbers.filter((num)=>num%2!=0)
let product = numbers.reduce((pro,num)=>pro*num,1)
let specified_length = words.filter((word)=>word.length>3)
console.log(cubes)
console.log(oddnumbers)
console.log(product)
console.log(specified_length)