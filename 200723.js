
//sum of n numbers

function sumofN(limit){
    let sum =0;
    let num = 1;
    while(num<=limit){
        sum= sum+num;
        num++
    }
        return sum
    }
    let result =sumofN(10);
    console.log("The sum of given number is: "+ result);

    //reverse a number

    function reverse_a_number(n)
{
	n = n + "";
	return n.split("").reverse().join("");
}
console.log(Number(reverse_a_number(1331)));

//count the number fo digits in the given number

function countofdigits(numbers){
    let count = 0;
    while(numbers!=0){
        count++; 
        numbers =Math.floor(numbers/10); 
    }
    return count
}
console.log(countofdigits(65879))

//sum of digits
// palidrome assignment
// reverse a string


function reverseString(str) {
    const charArray = str.split('');
    const reversedArray = charArray.reverse();
    const reversedStr = reversedArray.join('');
    return reversedStr;
}
const originalString = "Hello, world";
const reversedString = reverseString(originalString);
console.log(reversedString); 

//check whether the num is palindrome

function isPalindrome(number) {
    const numStr = number.toString();
    const reversedStr = numStr.split('').reverse().join('');
    return numStr === reversedStr;
}
console.log(isPalindrome(12321));
console.log(isPalindrome(12345));

//check whether the string is plaindrome

let checkPalindrome = (string) => {
    return string === string.split("").reverse().join("");
};
 
console.log("Is Palindrome? : " + checkPalindrome("noon"));
console.log("Is Palindrome?: " + checkPalindrome("apple"));

// check whether the num is palindrome

let checkthePalindrome = (numb) => {
    return numb === numb.split("").reverse().join("");
};
 
console.log("Is Palindrome? : " + checkthePalindrome("1331"));
console.log("Is Palindrome?: " + checkthePalindrome("5432"));

